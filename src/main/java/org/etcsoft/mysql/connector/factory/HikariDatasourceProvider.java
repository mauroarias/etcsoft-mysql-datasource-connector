package org.etcsoft.mysql.connector.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import org.etcsoft.mysql.connector.config.MysqlConfig;
import org.etcsoft.mysql.connector.repository.HikariMysqlDataSource;
import org.etcsoft.mysql.connector.repository.MysqlDataSource;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.DATABASE_ACCESS;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.WRONG_MYSQL_CONFIG;

public class HikariDatasourceProvider {
	public final static String MYSQL_DRIVER_NAME = "com.mysql.jdbc.Driver";
	public final static String HIKARI_POOL_NAME = "springHikariCP";
	public final static int DEFAULT_MAX_CONNECTION_POOL_SIZE = 5;
	public final static int DEFAULT_PREP_STMT_CACHE_SIZE = 250;
	public final static int DEFAULT_PREP_STMT_CACHE_SQL_LIMIT = 2048;

	private final static Logger logger = LoggerFactory.getLogger(HikariDatasourceProvider.class);
	private final Validator validation = new Validator();

	private final MysqlConfig mysqlConfig;
	private final ObjectMapper mapper;
	private final Integer maxConnectionPoolSize;
	private final Integer prepStmtCacheSize;
	private final Integer prepStmtCacheSqlLimit;

	public HikariDatasourceProvider(final MysqlConfig mysqlConfig,
									final ObjectMapper mapper) {
		this(mysqlConfig, mapper, null, null, null);
	}

	public HikariDatasourceProvider(final MysqlConfig mysqlConfig,
									final ObjectMapper mapper,
									final Integer maxConnectionPoolSize,
									final Integer prepStmtCacheSize,
									final Integer prepStmtCacheSqlLimit) {
		validation.throwIfInstanceNull(mysqlConfig, "Mysqlconfig cannot be null", WRONG_MYSQL_CONFIG);

		this.mysqlConfig = mysqlConfig;
		this.mapper = mapper;
		this.prepStmtCacheSize = prepStmtCacheSize == null ? DEFAULT_PREP_STMT_CACHE_SIZE : prepStmtCacheSize;
		this.maxConnectionPoolSize = maxConnectionPoolSize == null ?
				DEFAULT_MAX_CONNECTION_POOL_SIZE :
				maxConnectionPoolSize;
		this.prepStmtCacheSqlLimit = prepStmtCacheSqlLimit == null ?
				DEFAULT_PREP_STMT_CACHE_SQL_LIMIT :
				prepStmtCacheSqlLimit;
	}

	public MysqlDataSource getMysqlDatasource() {
		try {
			final com.zaxxer.hikari.HikariConfig hikariConfig = new com.zaxxer.hikari.HikariConfig();
			hikariConfig.setDriverClassName(MYSQL_DRIVER_NAME);
			hikariConfig.setJdbcUrl(String.format("jdbc:mysql://%s:%d/%s",
												  mysqlConfig.getHost(),
												  mysqlConfig.getPort(),
												  mysqlConfig.getDatabase()));
			hikariConfig.setUsername(this.mysqlConfig.getUser());
			hikariConfig.setPassword(mysqlConfig.getPassword());

			hikariConfig.setMaximumPoolSize(maxConnectionPoolSize);
			hikariConfig.setPoolName(HIKARI_POOL_NAME);

			hikariConfig.addDataSourceProperty("dataSource.cachePrepStmts", "true");
			hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSize",
											   prepStmtCacheSize.toString());
			hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSqlLimit",
											   prepStmtCacheSqlLimit.toString());
			hikariConfig.addDataSourceProperty("dataSource.useServerPrepStmts", "true");

			final HikariDataSource dataSource = new HikariDataSource(hikariConfig);
			dataSource.setAutoCommit(false);

			logger.info("connecting to Hikary mysql connector using config, {}", mysqlConfig.toString());

			return new HikariMysqlDataSource(dataSource, mapper);
		}
		catch(Exception cause) {
			throw new EtcsoftException(DATABASE_ACCESS,
									   format("Error connecting to mysql using Hikary connector, config: %s",
											  mysqlConfig.toString()),
									   cause);
		}
	}
}
