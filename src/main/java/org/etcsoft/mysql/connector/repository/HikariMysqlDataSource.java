package org.etcsoft.mysql.connector.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static java.lang.String.format;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.DATABASE_ACCESS;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.POJO_SERIALISING_ERROR;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;
import static org.etcsoft.tools.exception.UtilsErrorCode.UNKNOWN_ERROR;

public final class HikariMysqlDataSource implements MysqlDataSource {
	private final static Logger logger = LoggerFactory.getLogger(HikariMysqlDataSource.class);

	private final ObjectMapper mapper;
	private final Validator validation;
	private final HikariDataSource hikariDataSource;

	public HikariMysqlDataSource(final HikariDataSource hikariDataSource,
								 final ObjectMapper mapper) {
		this.validation = new Validator();

		validation.throwIfInstanceNull(hikariDataSource, "Mysql Hikari client cannot be null", INPUT_ERROR);
		validation.throwIfInstanceNull(mapper, "Object Mapper cannot be null", INPUT_ERROR);

		this.hikariDataSource = hikariDataSource;
		this.mapper = mapper;
	}

	@PreDestroy
	@SneakyThrows
	public void cleanUp() {
		close();
	}

	public <T> List<T> selectQueryList(final String query, final Class<T> clazz, final Object... params) {
		validation.throwIfInstanceBlank(query, "Query cannot be blank", INPUT_ERROR);
		validation.throwIfInstanceNull(clazz, "Class Type cannot be null", INPUT_ERROR);

		return executor((connection) -> pojoExecutor(fillParams(query, connection, params), clazz),
						query,
						format("Performing a select single row query: {}, in class: %s, params: %s",
									clazz.getTypeName(),
									params.toString()));
	}

	public <T> List<T> selectQueryList(final String query, final Class<T> clazz) {
		validation.throwIfInstanceBlank(query, "Query cannot be blank", INPUT_ERROR);
		validation.throwIfInstanceNull(clazz, "Class Type cannot be null", INPUT_ERROR);

		return executor((connection) -> pojoExecutor(fillParams(query, connection, null), clazz),
						query,
						format("Performing a select single row query: {}, in class: %s", clazz.getTypeName()));
	}

	public <T> T selectQuery(final String query, final Class<T> clazz, final Object... params) {
		validation.throwIfInstanceBlank(query, "Query cannot be blank", INPUT_ERROR);
		validation.throwIfInstanceNull(clazz, "Class Type cannot be null", INPUT_ERROR);

		return executor((connection) -> {
								 final List<T> result = pojoExecutor(fillParams(query, connection, params), clazz);
								 return result == null || result.isEmpty() ? null : result.get(0);
							 },
						query,
						format("Performing a select single row query: {}, in class: %s, params: %s",
									clazz.getTypeName(),
									params.toString()));
	}

	public <T> T selectQuery(final String query, final Class<T> clazz) {
		validation.throwIfInstanceBlank(query, "Query cannot be blank", INPUT_ERROR);
		validation.throwIfInstanceNull(clazz, "Class Type cannot be null", INPUT_ERROR);

		return executor((connection) -> {
								 final List<T> result = pojoExecutor(fillParams(query, connection, null), clazz);
								 return result == null || result.isEmpty() ? null : result.get(0);
							 },
						query,
						format("Performing a select single row query: {}, in class: %s",
									clazz.getTypeName()));
	}

	public Long selectSingleValue(final String query, final Object... params) {
		validation.throwIfInstanceBlank(query, "Query cannot be blank", INPUT_ERROR);

		return executor((connection) -> processSingleValue(connection, query, params),
						query,
						format("Performing a select count row query: {}, with params: %s", params.toString()));
	}

	public Long selectSingleValue(final String query) {
		validation.throwIfInstanceBlank(query, "Query cannot be blank", INPUT_ERROR);

		return executor((connection) -> processSingleValue(connection, query, null),
						query,
						"Performing a select count row query: {}");
	}

	public void executeQuery(final MysqlTransactionSupplier executor) {
		validation.throwIfInstanceNull(executor, "Executor query cannot be null", INPUT_ERROR);

		try(Connection connection = getDataSource().getConnection()) {
			try {
				logger.debug("Performing mysql transaction executor");
				executor.call(connection);
				connection.commit();
			}
			catch(SQLException cause) {
				rollback(connection);
				throw new EtcsoftException(DATABASE_ACCESS, "Error running mysql query", cause);
			}
			catch(EtcsoftException ex) {
				rollback(connection);
				throw ex;
			}
			catch(Exception cause) {
				rollback(connection);
				throw new EtcsoftException(UNKNOWN_ERROR, cause.getMessage(), cause);
			}
		}
		catch(SQLException cause) {
			throw new EtcsoftException(DATABASE_ACCESS, "Error running mysql query", cause);
		}
	}

	public CompletableFuture<Void> executeQueryAsync(MysqlTransactionSupplier executor) {
		return CompletableFuture.runAsync(() -> executeQuery(executor));
	}

	public <T> CompletableFuture<List<T>> selectQueryListAsync(String query, Class<T> clazz, Object... params) {
		return CompletableFuture.supplyAsync(() -> selectQueryList(query, clazz, params));
	}

	public <T> CompletableFuture<List<T>> selectQueryListAsync(String query, Class<T> clazz) {
		return CompletableFuture.supplyAsync(() -> selectQueryList(query, clazz));
	}

	public <T> CompletableFuture<T> selectQueryAsync(String query, Class<T> clazz, Object... params) {
		return CompletableFuture.supplyAsync(() -> selectQuery(query, clazz, params));
	}

	public <T> CompletableFuture<T> selectQueryAsync(String query, Class<T> clazz) {
		return CompletableFuture.supplyAsync(() -> selectQuery(query, clazz));
	}

	public CompletableFuture<Long> selectSingleValueAsync(String query, Object... params) {
		return CompletableFuture.supplyAsync(() -> selectSingleValue(query, params));
	}

	public CompletableFuture<Long> selectSingleValueAsync(String query) {
		return CompletableFuture.supplyAsync(() -> selectSingleValue(query));
	}

	public void close() throws Exception {
		getDataSource().close();
		logger.debug("closing mysql connection");
	}

	public HikariDataSource getDataSource() {
		if(hikariDataSource != null) {
			return hikariDataSource;
		}
		throw new EtcsoftException(DATABASE_ACCESS, "Mysql was not initialized properly, It cannot be null");
	}

	public boolean isAutocommit() {
		return getDataSource().isAutoCommit();
	}

	public void setAutocommit(final boolean autocommit) {
		getDataSource().setAutoCommit(autocommit);
	}

	@SneakyThrows
	private <T> List<T> pojoExecutor(final PreparedStatement selectStatement, final Class<T> clazz) {
		final ResultSet resultSet = selectStatement.executeQuery();

		final ResultSetMetaData metaData = resultSet.getMetaData();
		final int columns = metaData.getColumnCount();
		final List<T> list = new ArrayList<>();

		while(resultSet.next()) {
			final Map<String, Object> row = new HashMap<>();
			for(int i = 1; i <= columns; ++i) {
				validation.throwIfInstanceBlank(metaData.getColumnName(i),
												format("Error serialising pojo object, column name cannot be blank in index '%d'", i),
												POJO_SERIALISING_ERROR);
				row.put(metaData.getColumnName(i), resultSet.getObject(i));
			}
			list.add(mapper.convertValue(row, clazz));
		}
		return list;
	}

	@SneakyThrows
	private Long processSingleValue(Connection connection, String query, final Object[] params) {
		long count = 0;
		PreparedStatement stmt = fillParams(query, connection, params);
		final ResultSet resultSet = stmt.executeQuery();
		while(resultSet.next()) {
			count = resultSet.getLong(1);
		}
		return count;
	}

	@SneakyThrows
	private PreparedStatement fillParams(final String query, final Connection connection, final Object[] params) {
		final PreparedStatement preparedStatement = connection.prepareStatement(query);
		if(params != null) {
			int index = 1;
			for(Object param : params) {
				if(param != null) {
					preparedStatement.setObject(index++, param);
				}
			}
		}
		return preparedStatement;
	}

	private void rollback(Connection connection) {
		try {
			connection.rollback();
		} catch (Exception cause) {
			throw new EtcsoftException(DATABASE_ACCESS, "Error rolling back transaction", cause);
		}
	}

	private <T> T executor(final Supplier supplier, final String query, final String message) {
		try(Connection connection = getDataSource().getConnection()) {
			logger.debug(message, query);
			return (T) supplier.runQuery(connection);
		}
		catch(EtcsoftException ex) {
			throw ex;
		}
		catch(SQLException cause) {
			throw new EtcsoftException(DATABASE_ACCESS, format("Error selecting query: '%s'", query), cause);
		}
		catch(IllegalArgumentException cause) {
			throw new EtcsoftException(POJO_SERIALISING_ERROR,
									   format("Error pojo deserializer query: '%s'", query),
									   cause);
		}
		catch(Exception cause) {
			throw new EtcsoftException(UNKNOWN_ERROR, format("Error selecting query: '%s'", query), cause);
		}
	}

	@FunctionalInterface
	private interface Supplier<T> {
		T runQuery(Connection connection);
	}
}