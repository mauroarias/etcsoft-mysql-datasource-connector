package org.etcsoft.mysql.connector.repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface MysqlDataSource extends AutoCloseable
{
	DataSource getDataSource();
	boolean isAutocommit();
	void setAutocommit(boolean autocommit);
	void executeQuery(MysqlTransactionSupplier executor);
	<T> List<T> selectQueryList(String query, Class<T> clazz, Object... params);
	<T> List<T> selectQueryList(String query, Class<T> clazz);
	<T> T selectQuery(String query, Class<T> clazz, Object... params);
	<T> T selectQuery(String query, Class<T> clazz);
	Long selectSingleValue(String query, Object... params);
	Long selectSingleValue(String query);
	CompletableFuture<Void> executeQueryAsync(MysqlTransactionSupplier executor);
	<T> CompletableFuture<List<T>> selectQueryListAsync(String query, Class<T> clazz, Object... params);
	<T> CompletableFuture<List<T>> selectQueryListAsync(String query, Class<T> clazz);
	<T> CompletableFuture<T> selectQueryAsync(String query, Class<T> clazz, Object... params);
	<T> CompletableFuture<T> selectQueryAsync(String query, Class<T> clazz);
	CompletableFuture<Long> selectSingleValueAsync(String query, Object... params);
	CompletableFuture<Long> selectSingleValueAsync(String query);
}
