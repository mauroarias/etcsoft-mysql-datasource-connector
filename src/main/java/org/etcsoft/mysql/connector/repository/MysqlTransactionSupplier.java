package org.etcsoft.mysql.connector.repository;

import java.sql.Connection;
import java.sql.SQLException;

public interface MysqlTransactionSupplier {
	void call(Connection connection) throws SQLException;
}
