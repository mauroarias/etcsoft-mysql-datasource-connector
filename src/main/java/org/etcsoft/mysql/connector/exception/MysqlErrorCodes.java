package org.etcsoft.mysql.connector.exception;

import lombok.Getter;
import org.etcsoft.tools.exception.ErrorCode;
import org.etcsoft.tools.model.HttpStatus;

import static org.etcsoft.tools.model.HttpStatus.INTERNAL_SERVER_ERROR;

public enum MysqlErrorCodes implements ErrorCode {
	DATABASE_SCHEMA_ERROR(INTERNAL_SERVER_ERROR),
	POJO_SERIALISING_ERROR(INTERNAL_SERVER_ERROR),
	WRONG_MYSQL_CONFIG(INTERNAL_SERVER_ERROR),
	DATABASE_ACCESS(INTERNAL_SERVER_ERROR);

	@Getter
	private final HttpStatus httpStatus;

	MysqlErrorCodes(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
