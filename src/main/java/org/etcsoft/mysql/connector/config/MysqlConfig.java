package org.etcsoft.mysql.connector.config;

import lombok.Getter;
import lombok.ToString;
import org.etcsoft.tools.validation.Validator;

import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;

@ToString(exclude = {"password"})
public class MysqlConfig {

	private final static int DEFAULT_PORT = 3306;
	private final static String DEFAULT_DATABASE = "";

	@Getter
	private final String host;
	@Getter
	private final String database;
	@Getter
	private final String user;
	@Getter
	private final Integer port;
	@Getter
	private final String password;
	private final Validator validator = new Validator();

	public MysqlConfig(final String host,
					   final Integer port,
					   final String database,
					   final String user,
					   final String password) {
		validator.throwIfInstanceBlank(host, "Mysql host cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(user, "Mysql user cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceNull(password, "Mysql password cannot be null", INPUT_ERROR);
		this.host = host;
		this.port = port == null ? DEFAULT_PORT : port;
		this.database = database == null ? DEFAULT_DATABASE : database;
		this.user = user;
		this.password = password;
	}
}
