package org.etcsoft.mysql.connector.config;

import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;

public class MysqlConfigTest {

	@Test
	public void whenMysqlConfigOk_thenOk() {
		assertThat(new MysqlConfig("host", 3306, "database", "user", "password"))
				.hasFieldOrPropertyWithValue("host", "host")
				.hasFieldOrPropertyWithValue("port", 3306)
				.hasFieldOrPropertyWithValue("database", "database")
				.hasFieldOrPropertyWithValue("user", "user")
				.hasFieldOrPropertyWithValue("password", "password");
	}

	@Test
	public void whenPortNull_thenOk() {
		assertThat(new MysqlConfig("host", null, "database", "user", "password"))
				.hasFieldOrPropertyWithValue("host", "host")
				.hasFieldOrPropertyWithValue("port", 3306)
				.hasFieldOrPropertyWithValue("database", "database")
				.hasFieldOrPropertyWithValue("user", "user")
				.hasFieldOrPropertyWithValue("password", "password");
	}

	@Test
	public void whenPasswordEmpty_thenOk() {
		assertThat(new MysqlConfig("host", 3306, "database", "user", ""))
				.hasFieldOrPropertyWithValue("host", "host")
				.hasFieldOrPropertyWithValue("port", 3306)
				.hasFieldOrPropertyWithValue("database", "database")
				.hasFieldOrPropertyWithValue("user", "user")
				.hasFieldOrPropertyWithValue("password", "");
	}

	@Test
	public void whenDatabaseBlank_thenOk() {
		assertThat(new MysqlConfig("host", 3306, "", "user", "password"))
				.hasFieldOrPropertyWithValue("host", "host")
				.hasFieldOrPropertyWithValue("port", 3306)
				.hasFieldOrPropertyWithValue("database", "")
				.hasFieldOrPropertyWithValue("user", "user")
				.hasFieldOrPropertyWithValue("password", "password");

		assertThat(new MysqlConfig("host", 3306, null, "user", "password"))
				.hasFieldOrPropertyWithValue("host", "host")
				.hasFieldOrPropertyWithValue("port", 3306)
				.hasFieldOrPropertyWithValue("database", "")
				.hasFieldOrPropertyWithValue("user", "user")
				.hasFieldOrPropertyWithValue("password", "password");
	}

	@Test
	public void whenHostBlank_thenException() {
		assertThatThrownBy(() -> new MysqlConfig("", 3306, "database", "user", "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Mysql host cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> new MysqlConfig(null, 3306, "database", "user", "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Mysql host cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenUserBlank_thenException() {
		assertThatThrownBy(() -> new MysqlConfig("hostname", 3306, "database", "", "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Mysql user cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> new MysqlConfig("hostname", 3306, "database", null, "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Mysql user cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenPasswordNull_thenException() {
		assertThatThrownBy(() -> new MysqlConfig("hostname", 3306, "database", "user", null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Mysql password cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}
}
