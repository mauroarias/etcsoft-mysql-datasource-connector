package org.etcsoft.mysql.connector.factory;

import org.etcsoft.mysql.connector.config.MysqlConfig;
import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.WRONG_MYSQL_CONFIG;
import static org.mockito.Mockito.mock;

public class HikariDatasourceProviderTest {

	private MysqlConfig config = mock(MysqlConfig.class);

	@Test
	public void whenProviderOk_thenOk() {
		new HikariDatasourceProvider(config, null, 1, 2, 3);
	}

	@Test
	public void whenProviderDefaultValues_thenOk() {
		new HikariDatasourceProvider(config, null, null, null, null);
	}

	@Test
	public void whenNullConfig_thenException() {
		assertThatThrownBy(() -> new HikariDatasourceProvider(null, null, null, null, null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Mysqlconfig cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", WRONG_MYSQL_CONFIG)
				.hasNoCause();
	}
}
