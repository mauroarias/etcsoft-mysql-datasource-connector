package org.etcsoft.mysql.connector.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.assertj.core.api.AbstractThrowableAssert;
import org.etcsoft.tools.exception.ErrorCode;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.jackson.factory.ObjectMapperFactory;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.DATABASE_ACCESS;
import static org.etcsoft.mysql.connector.exception.MysqlErrorCodes.POJO_SERIALISING_ERROR;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;
import static org.etcsoft.tools.exception.UtilsErrorCode.UNKNOWN_ERROR;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HikaryMysqlDataSourceTest
{
	private static ObjectMapper mapper = new ObjectMapperFactory().objectMapperFactory();
	
	private HikariDataSource dataSource = mock(HikariDataSource.class);
	private Connection connection = mock(Connection.class);
	private PreparedStatement preparedStatement = mock(PreparedStatement.class);
	private ResultSet resultSet = mock(ResultSet.class);
	private ResultSetMetaData metaData = mock(ResultSetMetaData.class);
	private MysqlDataSource hikariMysqlConnector = new HikariMysqlDataSource(dataSource, mapper);

	@Test
	@SneakyThrows
	public void whenSelectListQuery_thenOk() {
		mockQueryList();

		assertThat(hikariMysqlConnector.selectQueryList("my query", PojoClassBeanTest.class))
				.contains(PojoClassBeanTest
								  .builder().id(5L).name("yy").value(15.5).build(),
						  PojoClassBeanTest
								  .builder().id(4L).name("xx").value(33.6).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectListQueryWithParams_thenOk() {
		mockQueryList();

		assertThat(hikariMysqlConnector.selectQueryList("my query", PojoClassBeanTest.class, "1", "2"))
				.contains(PojoClassBeanTest
								  .builder().id(5L).name("yy").value(15.5).build(),
						  PojoClassBeanTest
								  .builder().id(4L).name("xx").value(33.6).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectQuery_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L);
		when(resultSet.getObject(eq(2))).thenReturn("yy");
		when(resultSet.getObject(eq(3))).thenReturn(15.5);

		assertThat(hikariMysqlConnector.selectQuery("my query", PojoClassBeanTest.class))
				.isEqualTo(PojoClassBeanTest.builder().id(5L).name("yy").value(15.5).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectQueryWithParams_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L);
		when(resultSet.getObject(eq(2))).thenReturn("yy");
		when(resultSet.getObject(eq(3))).thenReturn(15.5);

		assertThat(hikariMysqlConnector.selectQuery("my query", PojoClassBeanTest.class, "1", "2"))
				.isEqualTo(PojoClassBeanTest.builder().id(5L).name("yy").value(15.5).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectSingleValue_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThat(hikariMysqlConnector.selectSingleValue("my query")).isEqualTo(1L);
	}

	@Test
	@SneakyThrows
	public void whenSelectSingleValueWithParams_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(resultSet.getLong(anyInt())).thenReturn(1000L);

		assertThat(hikariMysqlConnector.selectSingleValue("my query", "1", 2)).isEqualTo(1000L);
	}

	@Test
	@SneakyThrows
	public void whenDatasourceNull_thenException() {
		assertThatThrownBy(() -> new HikariMysqlDataSource(null, mapper))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Mysql Hikari client cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	@SneakyThrows
	public void whenMapperNull_thenException() {
		assertThatThrownBy(() -> new HikariMysqlDataSource(dataSource, null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Object Mapper cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	@SneakyThrows
	public void whenGetConnectionException_thenException() {
		final String exceptionMessage = "connection exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownByExecuteQuery(DATABASE_ACCESS,
									   "Error running mysql query",
									   "my query").hasCauseInstanceOf(SQLException.class);
	}

	@Test
	@SneakyThrows
	public void whenPrepareStatementException_thenException() {
		final String exceptionMessage = "prepare statement exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownByExecuteQuery(DATABASE_ACCESS,
									   format("Error running mysql query"),
									   "my query").hasCauseInstanceOf(SQLException.class);
	}

	@Test
	@SneakyThrows
	public void whenSetObjectException_then() {
		final String exceptionMessage = "setObject exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		doThrow(new SQLException(exceptionMessage))
				.when(preparedStatement)
				.setObject(anyInt(), any(Object.class));
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(false);

		hikariMysqlConnector.selectQueryList("my query", PojoClassBeanTest.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.selectQuery("my query", PojoClassBeanTest.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.selectSingleValue("my query");
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryException_then() {
		final String exceptionMessage = "execute query exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenGetMetadataException_then() {
		final String exceptionMessage = "get metadata exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.selectSingleValue("my query");
		hikariMysqlConnector.selectSingleValue("my query", "1", "2");
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenGetColumnCountException_then() {
		final String exceptionMessage = "getcolumnCount exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.selectSingleValue("my query");
		hikariMysqlConnector.selectSingleValue("my query", "1", "2");
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenGetNextRowException_then() {
		final String exceptionMessage = "next exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySingleValue(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenColumnNameException_then() {
		final String exceptionMessage = "getColumnName exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(anyInt())).thenThrow(new SQLException(exceptionMessage));
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQueryList(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "my query"), "my query")
				.hasCauseInstanceOf(SQLException.class);
		assertThatThrownBySelectQuery(DATABASE_ACCESS, format(message, "with params"), "with params", "1", "2")
				.hasCauseInstanceOf(SQLException.class);
		hikariMysqlConnector.selectSingleValue("my query");
		hikariMysqlConnector.selectSingleValue("my query", "1", "2");
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenGetColumnNameNullException_then() {
		final String message = "Error serialising pojo object, column name cannot be blank in index '1'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn(null);
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryList(POJO_SERIALISING_ERROR, message, "my query").hasNoCause();
		assertThatThrownBySelectQueryList(POJO_SERIALISING_ERROR, message, "with params", "1", "2").hasNoCause();
		assertThatThrownBySelectQuery(POJO_SERIALISING_ERROR, message, "my query").hasNoCause();
		assertThatThrownBySelectQuery(POJO_SERIALISING_ERROR, message, "with params", "1", "2").hasNoCause();
		hikariMysqlConnector.selectSingleValue("my query");
		hikariMysqlConnector.selectSingleValue("my query", "1", "2");
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenGetColumnEmptyException_then() {
		final String message = "Error serialising pojo object, column name cannot be blank in index '1'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("");
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryList(POJO_SERIALISING_ERROR, message, "my query").hasNoCause();
		assertThatThrownBySelectQueryList(POJO_SERIALISING_ERROR, message, "with params", "1", "2").hasNoCause();
		assertThatThrownBySelectQuery(POJO_SERIALISING_ERROR, message, "my query").hasNoCause();
		assertThatThrownBySelectQuery(POJO_SERIALISING_ERROR, message, "with params", "1", "2").hasNoCause();
		hikariMysqlConnector.selectSingleValue("my query");
		hikariMysqlConnector.selectSingleValue("my query", "1", "2");
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenConvertValueException_then() {
		final String message = "Error pojo deserializer query: 'my query'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn("fff");
		when(resultSet.getObject(eq(2))).thenReturn("yy");
		when(resultSet.getObject(eq(3))).thenReturn(15.5);
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryList(POJO_SERIALISING_ERROR, message, "my query")
				.hasCauseInstanceOf(IllegalArgumentException.class);
		assertThatThrownBySelectQueryList(POJO_SERIALISING_ERROR, message, "my query", "1", "2")
				.hasCauseInstanceOf(IllegalArgumentException.class);
		assertThatThrownBySelectQuery(POJO_SERIALISING_ERROR, message, "my query")
				.hasCauseInstanceOf(IllegalArgumentException.class);
		assertThatThrownBySelectQuery(POJO_SERIALISING_ERROR, message, "my query", "1", "2")
				.hasCauseInstanceOf(IllegalArgumentException.class);
		hikariMysqlConnector.selectSingleValue("my query");
		hikariMysqlConnector.selectSingleValue("my query", "1", "2");
		hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement("my query"));
	}

	@Test
	@SneakyThrows
	public void whenSelectQueryListWithValueNull_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L, 4L);
		when(resultSet.getObject(eq(2))).thenReturn(null, "xx");
		when(resultSet.getObject(eq(3))).thenReturn(15.5, 33.6);

		assertThat(hikariMysqlConnector.selectQueryList("my query", PojoClassBeanTest.class))
				.contains(PojoClassBeanTest
								  .builder().id(5L).value(15.5).build(),
						  PojoClassBeanTest
								  .builder().id(4L).name("xx").value(33.6).build());
	}

	@Test
	@SneakyThrows
	public void whenExecuteQuery_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(true);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);

		hikariMysqlConnector.executeQuery((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.close();
		});
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryAutoCommitFalse_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);

		hikariMysqlConnector.executeQuery((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.close();
		});
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryFail_then() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		doThrow(new SQLException("rolling back!!!")).when(statement).execute();

		assertThatThrownBy(() -> hikariMysqlConnector.executeQuery((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.execute();
		}))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Error running mysql query")
				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
				.hasCauseInstanceOf(SQLException.class);
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryFailNotSqlException_then() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);

		assertThatThrownBy(() -> hikariMysqlConnector.executeQuery((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			Integer.parseInt("throw exception");
		}))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("For input string: \"throw exception\"")
				.hasFieldOrPropertyWithValue("errorCode", UNKNOWN_ERROR)
				.hasCauseInstanceOf(NumberFormatException.class);
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryFailRollbackFail_then() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		doThrow(new SQLException("rollback error!!!")).when(connection).rollback();
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		doThrow(new SQLException("rolling back!!!")).when(statement).execute();

		assertThatThrownBy(() -> hikariMysqlConnector.executeQuery((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.execute();
		}))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Error rolling back transaction")
				.hasFieldOrPropertyWithValue("errorCode", DATABASE_ACCESS)
				.hasCauseInstanceOf(SQLException.class);
	}

	@Test
	public void whenSelectQueryListWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList("", PojoClassBeanTest.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList(null, PojoClassBeanTest.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectQueryListWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList("my Query", null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Class Type cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectQueryListParamWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList("", PojoClassBeanTest.class, "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList(null, PojoClassBeanTest.class, "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectQueryListParamWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList("my Query", null, "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Class Type cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectQueryWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQuery("", PojoClassBeanTest.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> hikariMysqlConnector.selectQuery(null, PojoClassBeanTest.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectQueryWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQuery("my Query", null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Class Type cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectQueryParamWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQuery("", PojoClassBeanTest.class, "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> hikariMysqlConnector.selectQuery(null, PojoClassBeanTest.class, "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectQueryParamWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQuery("my Query", null, "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Class Type cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectSingleValueWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValue(""))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValue(null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenSelectSingleValueParamWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValue("", "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValue(null, "1", "2"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Query cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenExcecuteQueryNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.executeQuery(null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Executor query cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	private AbstractThrowableAssert assertThatThrownBySelectQueryList(final ErrorCode errorCode,
																	  final String exceptionMessage,
																	  final String query) {
		return assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList(query, PojoClassBeanTest.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith(exceptionMessage)
				.hasFieldOrPropertyWithValue("errorCode", errorCode);
	}

	private AbstractThrowableAssert assertThatThrownBySelectQueryList(final ErrorCode errorCode,
																	  final String exceptionMessage,
																	  final String query,
																	  Object ... params) {
		return assertThatThrownBy(() -> hikariMysqlConnector.selectQueryList(query, PojoClassBeanTest.class, params))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith(exceptionMessage)
				.hasFieldOrPropertyWithValue("errorCode", errorCode);
	}

	private AbstractThrowableAssert assertThatThrownBySelectQuery(final ErrorCode errorCode,
																  final String exceptionMessage,
																  final String query) {
		return assertThatThrownBy(() -> hikariMysqlConnector.selectQuery(query, PojoClassBeanTest.class))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith(exceptionMessage)
				.hasFieldOrPropertyWithValue("errorCode", errorCode);
	}

	private AbstractThrowableAssert assertThatThrownBySelectQuery(final ErrorCode errorCode,
																  final String exceptionMessage,
																  final String query,
																  final Object ... params) {
		return assertThatThrownBy(() -> hikariMysqlConnector.selectQuery(query, PojoClassBeanTest.class, params))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith(exceptionMessage)
				.hasFieldOrPropertyWithValue("errorCode", errorCode);
	}

	private AbstractThrowableAssert assertThatThrownBySingleValue(final ErrorCode errorCode,
																  final String exceptionMessage,
																  final String query) {
		return assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValue(query))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith(exceptionMessage)
				.hasFieldOrPropertyWithValue("errorCode", errorCode);
	}

	private AbstractThrowableAssert assertThatThrownBySingleValue(final ErrorCode errorCode,
																  final String exceptionMessage,
																  final String query,
																  final Object ... params) {
		return assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValue(query, params))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith(exceptionMessage)
				.hasFieldOrPropertyWithValue("errorCode", errorCode);
	}

	private AbstractThrowableAssert assertThatThrownByExecuteQuery(final ErrorCode errorCode,
																   final String exceptionMessage,
																   final String query) {
		return assertThatThrownBy(() -> hikariMysqlConnector.executeQuery((connection) -> connection.prepareStatement(query)))
				.isInstanceOf(EtcsoftException.class)
				.hasMessageStartingWith(exceptionMessage)
				.hasFieldOrPropertyWithValue("errorCode", errorCode);
	}

	@SneakyThrows
	private void mockQueryList() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L, 4L);
		when(resultSet.getObject(eq(2))).thenReturn("yy", "xx");
		when(resultSet.getObject(eq(3))).thenReturn(15.5, 33.6);
	}
}
