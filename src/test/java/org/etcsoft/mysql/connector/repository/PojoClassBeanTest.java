package org.etcsoft.mysql.connector.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PojoClassBeanTest {
	private Long id;
	private String name;
	private Double value;
}
