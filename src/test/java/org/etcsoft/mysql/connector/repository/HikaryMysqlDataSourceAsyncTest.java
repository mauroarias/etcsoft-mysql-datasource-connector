package org.etcsoft.mysql.connector.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.jackson.factory.ObjectMapperFactory;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.concurrent.CompletionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HikaryMysqlDataSourceAsyncTest
{
	private static ObjectMapper mapper = new ObjectMapperFactory().objectMapperFactory();
	
	private HikariDataSource dataSource = mock(HikariDataSource.class);
	private Connection connection = mock(Connection.class);
	private PreparedStatement preparedStatement = mock(PreparedStatement.class);
	private ResultSet resultSet = mock(ResultSet.class);
	private ResultSetMetaData metaData = mock(ResultSetMetaData.class);
	private MysqlDataSource hikariMysqlConnector = new HikariMysqlDataSource(dataSource, mapper);

	@Test
	@SneakyThrows
	public void whenSelectListAsyncQuery_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L, 4L);
		when(resultSet.getObject(eq(2))).thenReturn("yy", "xx");
		when(resultSet.getObject(eq(3))).thenReturn(15.5, 33.6);

		assertThat(hikariMysqlConnector.selectQueryListAsync("my query", PojoClassBeanTest.class).join())
				.contains(PojoClassBeanTest
								  .builder().id(5L).name("yy").value(15.5).build(),
						  PojoClassBeanTest
								  .builder().id(4L).name("xx").value(33.6).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectListAsyncQueryWithParams_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L, 4L);
		when(resultSet.getObject(eq(2))).thenReturn("yy", "xx");
		when(resultSet.getObject(eq(3))).thenReturn(15.5, 33.6);

		assertThat(hikariMysqlConnector.selectQueryListAsync("my query", PojoClassBeanTest.class, "1", "2").join())
				.contains(PojoClassBeanTest
								  .builder().id(5L).name("yy").value(15.5).build(),
						  PojoClassBeanTest
								  .builder().id(4L).name("xx").value(33.6).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectQueryAsync_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L);
		when(resultSet.getObject(eq(2))).thenReturn("yy");
		when(resultSet.getObject(eq(3))).thenReturn(15.5);

		assertThat(hikariMysqlConnector.selectQueryAsync("my query", PojoClassBeanTest.class).join())
				.isEqualTo(PojoClassBeanTest.builder().id(5L).name("yy").value(15.5).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectQueryAsyncWithParams_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L);
		when(resultSet.getObject(eq(2))).thenReturn("yy");
		when(resultSet.getObject(eq(3))).thenReturn(15.5);

		assertThat(hikariMysqlConnector.selectQueryAsync("my query", PojoClassBeanTest.class, "1", "2").join())
				.isEqualTo(PojoClassBeanTest.builder().id(5L).name("yy").value(15.5).build());
	}

	@Test
	@SneakyThrows
	public void whenSelectSingleValueAsync_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThat(hikariMysqlConnector.selectSingleValueAsync("my query").join()).isEqualTo(1L);
	}

	@Test
	@SneakyThrows
	public void whenSelectSingleValueAsyncWithParams_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, false);
		when(resultSet.getLong(anyInt())).thenReturn(1000L);

		assertThat(hikariMysqlConnector.selectSingleValueAsync("my query", "1", 2).join()).isEqualTo(1000L);
	}

	@Test
	@SneakyThrows
	public void whenGetConnectionException_thenException() {
		final String exceptionMessage = "connection exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		assertThatThrownBySingleValueAsync("my query");
		assertThatThrownBySingleValueAsync("with params", "1", "2");
		assertThatThrownByExecuteQueryAsync("my query");
	}

	@Test
	@SneakyThrows
	public void whenPrepareStatementException_thenException() {
		final String exceptionMessage = "prepare statement exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		assertThatThrownBySingleValueAsync("my query");
		assertThatThrownBySingleValueAsync("with params", "1", "2");
		assertThatThrownByExecuteQueryAsync("my query");
	}

	@Test
	@SneakyThrows
	public void whenSetObjectException_then() {
		final String exceptionMessage = "setObject exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		doThrow(new SQLException(exceptionMessage))
				.when(preparedStatement)
				.setObject(anyInt(), any(Object.class));
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(false);

		hikariMysqlConnector.selectQueryListAsync("my query", PojoClassBeanTest.class).join();
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		hikariMysqlConnector.selectQueryAsync("my query", PojoClassBeanTest.class).join();
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		hikariMysqlConnector.selectSingleValueAsync("my query").join();
		assertThatThrownBySingleValueAsync("with params", "1", "2");
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryException_then() {
		final String exceptionMessage = "execute query exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		assertThatThrownBySingleValueAsync("my query");
		assertThatThrownBySingleValueAsync("with params", "1", "2");
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenGetMetadataException_then() {
		final String exceptionMessage = "get metadata exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		hikariMysqlConnector.selectSingleValueAsync("my query").join();
		hikariMysqlConnector.selectSingleValueAsync("my query", "1", "2").join();
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenGetColumnCountException_then() {
		final String exceptionMessage = "getcolumnCount exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		hikariMysqlConnector.selectSingleValueAsync("my query").join();
		hikariMysqlConnector.selectSingleValueAsync("my query", "1", "2").join();
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenGetNextRowException_then() {
		final String exceptionMessage = "next exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenThrow(new SQLException(exceptionMessage));

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		assertThatThrownBySingleValueAsync("my query");
		assertThatThrownBySingleValueAsync("with params", "1", "2");
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenColumnNameException_then() {
		final String exceptionMessage = "getColumnName exception!!!";
		final String message = "Error selecting query: '%s'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(anyInt())).thenThrow(new SQLException(exceptionMessage));
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		hikariMysqlConnector.selectSingleValueAsync("my query").join();
		hikariMysqlConnector.selectSingleValueAsync("my query", "1", "2").join();
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenGetColumnNameNullException_then() {
		final String message = "Error serialising pojo object, column name cannot be blank in index '1'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn(null);
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		hikariMysqlConnector.selectSingleValueAsync("my query").join();
		hikariMysqlConnector.selectSingleValueAsync("my query", "1", "2").join();
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenGetColumnEmptyException_then() {
		final String message = "Error serialising pojo object, column name cannot be blank in index '1'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("");
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("with params", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("with params", "1", "2");
		hikariMysqlConnector.selectSingleValueAsync("my query").join();
		hikariMysqlConnector.selectSingleValueAsync("my query", "1", "2").join();
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenConvertValueException_then() {
		final String message = "Error pojo deserializer query: 'my query'";
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, true, true, true, false, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn("fff");
		when(resultSet.getObject(eq(2))).thenReturn("yy");
		when(resultSet.getObject(eq(3))).thenReturn(15.5);
		when(resultSet.getLong(anyInt())).thenReturn(1L);

		assertThatThrownBySelectQueryListAsync("my query");
		assertThatThrownBySelectQueryListAsync("my query", "1", "2");
		assertThatThrownBySelectQueryAsync("my query");
		assertThatThrownBySelectQueryAsync("my query", "1", "2");
		hikariMysqlConnector.selectSingleValueAsync("my query").join();
		hikariMysqlConnector.selectSingleValueAsync("my query", "1", "2").join();
		hikariMysqlConnector.executeQueryAsync((connection) -> connection.prepareStatement("my query")).join();
	}

	@Test
	@SneakyThrows
	public void whenSelectQueryListWithValueNull_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);
		when(resultSet.getMetaData()).thenReturn(metaData);
		when(metaData.getColumnCount()).thenReturn(3);
		when(resultSet.next()).thenReturn(true, true, false);
		when(metaData.getColumnName(eq(1))).thenReturn("id");
		when(metaData.getColumnName(eq(2))).thenReturn("name");
		when(metaData.getColumnName(eq(3))).thenReturn("value");
		when(resultSet.getObject(eq(1))).thenReturn(5L, 4L);
		when(resultSet.getObject(eq(2))).thenReturn(null, "xx");
		when(resultSet.getObject(eq(3))).thenReturn(15.5, 33.6);

		assertThat(hikariMysqlConnector.selectQueryListAsync("my query", PojoClassBeanTest.class).join())
				.contains(PojoClassBeanTest
								  .builder().id(5L).value(15.5).build(),
						  PojoClassBeanTest
								  .builder().id(4L).name("xx").value(33.6).build());
	}

	@Test
	@SneakyThrows
	public void whenExecuteQuery_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(true);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);

		hikariMysqlConnector.executeQueryAsync((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.close();
		}).join();
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryAutoCommitFalse_thenOk() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);

		hikariMysqlConnector.executeQueryAsync((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.close();
		}).join();
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryFail_then() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		doThrow(new SQLException("rolling back!!!")).when(statement).execute();

		assertThatThrownBy(() -> hikariMysqlConnector.executeQueryAsync((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.execute();
		}).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryFailNotSqlException_then() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);

		assertThatThrownBy(() -> hikariMysqlConnector.executeQueryAsync((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			Integer.parseInt("throw exception");
		}).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryFailRollbackFail_then() {
		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.getAutoCommit()).thenReturn(false);
		doThrow(new SQLException("rollback error!!!")).when(connection).rollback();
		final PreparedStatement statement = mock(PreparedStatement.class);
		when(connection.prepareStatement(any(String.class))).thenReturn(statement);
		doThrow(new SQLException("rolling back!!!")).when(statement).execute();

		assertThatThrownBy(() -> hikariMysqlConnector.executeQueryAsync((conn) -> {
			conn.prepareStatement("query");
			assertThat(conn).isSameAs(connection);
			statement.execute();
		}).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryListWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryListAsync("", PojoClassBeanTest.class).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);

		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryListAsync(null, PojoClassBeanTest.class).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryListWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryListAsync("my Query", null).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryListParamWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryListAsync("", PojoClassBeanTest.class, "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);

		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryListAsync(null, PojoClassBeanTest.class, "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryListParamWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryListAsync("my Query", null, "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryAsync("", PojoClassBeanTest.class).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);

		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryAsync(null, PojoClassBeanTest.class).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryAsync("my Query", null).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryParamWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryAsync("", PojoClassBeanTest.class, "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);

		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryAsync(null, PojoClassBeanTest.class, "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectQueryParamWithClassNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryAsync("my Query", null, "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectSingleValueWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValueAsync("").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);

		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValueAsync(null).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenSelectSingleValueParamWithQueryBlank_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValueAsync("", "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);

		assertThatThrownBy(() -> hikariMysqlConnector.selectSingleValueAsync(null, "1", "2").join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	@Test
	public void whenExcecuteQueryNull_thenException() {
		assertThatThrownBy(() -> hikariMysqlConnector.executeQueryAsync(null).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	private void assertThatThrownBySelectQueryListAsync(final String query) {
		assertThatThrownBy(() -> hikariMysqlConnector.selectQueryListAsync(query, PojoClassBeanTest.class).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	private void assertThatThrownBySelectQueryListAsync(final String query, Object ... params) {
		assertThatThrownBy(() -> hikariMysqlConnector
				.selectQueryListAsync(query, PojoClassBeanTest.class, params).join())
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	private void assertThatThrownBySelectQueryAsync(final String query) {
		assertThatThrownBy(() -> hikariMysqlConnector
				.selectQueryAsync(query, PojoClassBeanTest.class).join())
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	private void assertThatThrownBySelectQueryAsync(final String query, final Object ... params) {
		assertThatThrownBy(() -> hikariMysqlConnector
				.selectQueryAsync(query, PojoClassBeanTest.class, params).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	private void assertThatThrownBySingleValueAsync(final String query) {
		assertThatThrownBy(() -> hikariMysqlConnector
				.selectSingleValueAsync(query).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	private void assertThatThrownBySingleValueAsync(final String query, final Object ... params) {
		assertThatThrownBy(() -> hikariMysqlConnector
				.selectSingleValueAsync(query, params).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}

	private void assertThatThrownByExecuteQueryAsync(final String query) {
		assertThatThrownBy(() -> hikariMysqlConnector
				.executeQueryAsync((connection) -> connection.prepareStatement(query)).join())
				.isInstanceOf(CompletionException.class)
				.hasCauseInstanceOf(EtcsoftException.class);
	}
}
