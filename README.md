# HikariCP Mysql Datasource Connector

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-mysql-datasource-connector/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.etcsoft/etcsoft-mysql-datasource-connector)
[![Build Status](https://travis-ci.org/mauroarias/etcsoft-mysql-datasource-connector.svg?branch=master)](https://travis-ci.org/mauroarias/etcsoft-mysql-datasource-connector)

This library gives access to Mysql using hikariCp.

# configuration
* **spring.mysql.hikaricp** this property create an **MysqlDatasource instance**
* **mysql.host:** mysql host. By default dev "localhost" 
* **mysql.port:** mysql port. By default 3306
* **mysql.database:** mysql database. By default schema. 
* **mysql.user:** mysql "user". By default root 
* **mysql.password:** mysql "password. By default admin

```
private final String host = "my host";
private final Integer port = "3306";
private final String database = "db";
private final String user = "user";
private final String password = "password";
private final MysqlConfig config = new MysqlConfig(host, port, database, user, password);
privtae final ObjectMapper objectMapper = new ObjectMapperFactory().objectMapperFactory();
private HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(config, obectMapper);
HikariDataSource dataSource = dataSourceProvider.getMysqlDatasource();
```

Using extra parameters:
* **mysql.max.connection.pool.size:** max number of connections in mysql. by default 5 
* **mysql.prep.stmt.cache.size** prepare statement cache size. by default 250
* **mysql.prep.stmt.cache.sql.limit** prepare statement cache limit size. by default 2048


```
//Mysql configuration
final String mysqlHost = "localhost";
final Integer mysqlPort = 3306;
final String myDB = "schema";
final String myUser = "root";
final String myPassword = "Admin"; 
final MysqlConfig mysqlConfig = new MysqlConfig(mysqlHost, mysqlPort, myDB, myUser, myPassword);

//Hikari Constructor
//Another Object mapper constructor can be also used
final ObjectMapper mapper = new ObjectMapperConfig().objectMapperFactory();
final Integer maxConnectionPoolSize = 5;
final Integer prepStmtCacheSize = 250;
final Integer prepStmtCacheSqlLimit = 2048;
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper, maxConnectionPoolSize, prepStmtCacheSize, prepStmtCacheSqlLimit);
final MysqlDataSource = dataSourceProvider.getMysqlConnector();
```

## Usage:

* GET DATASOURCE:
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final DataSource dataSource = mysqlDataSource.getDataSource();
```

* IS AUTOCOMMIT:
It returns the status of autocommit.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
mysqlDataSource.isAutocommit();
```

* SET AUTOCOMMIT STATUS:
It sets the status of autocommit.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final boolean autocommit = false;
mysqlDataSource.setAutocommit(autocommit);
```


### SYNC CALLS:

* EXECUTE QUERY:
It opens a new transaction and execute a query into the executor and committed at the end. Note that a rollback is performed if something is wrong.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
mysqlDataSource.executeQuery((connection) -> {
    final String insertQuery = "INSERT INTO profile VALUES (?,'execute','1966-05-12',?);";
    PreparedStatement stmt = connection.prepareStatement(insertQuery);
    stmt.setLong(1, id);
    stmt.setBoolean(2,true);
    stmt.execute();
});
```

* SELECT QUERY LIST:
It returns a list of instances the type specified, according to the query executed. Note that inner objects are not serialized
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile;
final List<Profile> profiles = mysqlDataSource.selectQueryList(query, Profile.class);
```

* SELECT QUERY WITH PARAMETERS LIST:
It returns a list of instances the type specified, according to the query with parameters executed. Note that inner objects are not serialized
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile WHERE id < ? AND name = ?;
final Long id = 100;
final String name = "my name";
final List<Profile> profiles = mysqlDataSource.selectQueryList(query, Profile.class, id, name);
```

* SELECT QUERY:
It returns an instance the type specified, according to the query executed. Note that inner objects are not serialized. if there are more that one object matching in the query only the first object is returned.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile WHERE id = 5;
final Profile profile = mysqlDataSource.selectQuery(query, Profile.class);
```

* SELECT QUERY WITH PARAMETERS:
It returns a list of instances the type specified, according to the query with parameters executed. Note that inner objects are not serialized
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile WHERE id = ?;
final Long id = 5;
final Profile profile = mysqlDataSource.selectQuery(query, Profile.class, id);
```

* SELECT SINGLE VALUE QUERY:
It returns a single value as result of the query.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT count(*) FROM profile WHERE id = 5;
final Long count = mysqlDataSource.selectSingleValue(query);
```

* SELECT SINGLE VALUE QUERY WITH PARAMETERS:
It returns a single value as result of the query.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT count(*) FROM profile WHERE id < ?;
final Long id = 500;
final Long count = mysqlDataSource.selectSingleValue(query, id);
```

### ASYNC CALLS:

* EXECUTE QUERY:
It opens a new transaction and execute a query into the executor and committed at the end. Note that a rollback is performed if something is wrong.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
CompletableFuture<Void> cf = mysqlDataSource.executeQueryAsync((connection) -> {
    final String insertQuery = "INSERT INTO profile VALUES (?,'execute','1966-05-12',?);";
    PreparedStatement stmt = connection.prepareStatement(insertQuery);
    stmt.setLong(1, id);
    stmt.setBoolean(2,true);
    stmt.execute();
});
```

* SELECT QUERY LIST:
It returns a list of instances the type specified, according to the query executed. Note that inner objects are not serialized
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile;
final CompletableFuture<List<Profile>> cf = mysqlDataSource.selectQueryListAsync(query, Profile.class);
```

* SELECT QUERY WITH PARAMETERS LIST:
It returns a list of instances the type specified, according to the query with parameters executed. Note that inner objects are not serialized
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile WHERE id < ? AND name = ?;
final Long id = 100;
final String name = "my name";
final CompletableFuture<List<Profile>> cf = mysqlDataSource.selectQueryListAsync(query, Profile.class, id, name);
```

* SELECT QUERY:
It returns an instance the type specified, according to the query executed. Note that inner objects are not serialized. if there are more that one object matching in the query only the first object is returned.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile WHERE id = 5;
final CompletableFuture<Profile> cf = mysqlDataSource.selectQueryAsync(query, Profile.class);
```

* SELECT QUERY WITH PARAMETERS:
It returns a list of instances the type specified, according to the query with parameters executed. Note that inner objects are not serialized
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT * FROM profile WHERE id = ?;
final Long id = 5;
final CompletableFuture<Profile> cf = mysqlDataSource.selectQueryAsync(query, Profile.class, id);
```

* SELECT SINGLE VALUE QUERY:
It returns a single value as result of the query.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT count(*) FROM profile WHERE id = 5;
final CompletableFuture<Long> cf = mysqlDataSource.selectSingleValueAsync(query);
```

* SELECT SINGLE VALUE QUERY WITH PARAMETERS:
It returns a single value as result of the query.
```
...
final HikariDatasourceProvider dataSourceProvider = new HikariDatasourceProvider(mysqlConfig, mapper);
final HikariDataSource mysqlDataSource = dataSourceProvider.getMysqlConnector();
final String query = "SELECT count(*) FROM profile WHERE id < ?;
final Long id = 500;
final CompletableFuture<Long> cf = mysqlDataSource.selectSingleValueAsync(query, id);
```

## Compiling 

### running unit test
```
mvn clean install
```

### skipping unit tests
```
mvn clean install -DskipTests
```
